# Conway's Game of Life

Video Demo: https://youtu.be/uDWOk9fv3T0

For this project I implemented a program that plays Conway’s Game of Life. The game is a “zero player game” which evolves from its initial configuration according to a set of fixed rules. In Conway’s Game of Life, entities live, die, or are born based on their surrounding neighbors.

The rules of the game are as follows:

- A cell with zero or one neighbors dies from loneliness
- A cell with four or more neighbors dies due to overpopulation
- A dead cell with exactly three live neighbors becomes alive

The game world is a torus represented by a 2D array of values. Live cells are represented by “@“ and dead cells are represented by “.”. At each discrete time step, every cell in the 2D grid gets a new value based on the current value of its eight neighbors.
