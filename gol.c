/**
 * Game of Life
 * 2D board of "live @" and "dead ." elements
 * 
 * Rules
 ** Live cell with 0 or 1 live neighbors dies
 ** Live cell with more than 4 neighbors dies
 ** Dead cell with 3 neighbors becomes alive 
 *
 *The board is configured from an input file with the following format:
 * num rows
 * num cols
 * num iterations
 * num of following coordinate pairs (set each (i, j) value to 1
 * i j
 * i j
 * �
 * �
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>

int* getInitialConditions(char* argv[], int*rows, int*columns, int*num_iterations, int*num_initial_elements, int*display);
char* initializeBoard(int rows, int columns, int* indicies, int num_initial_elements);
void printBoard(int rows, int columns, char* game_board);
void eightNeighbors(int* n1, int* n2, int* n3, int* n4, int* n5, int* n6, int* n7, int* n8, char* game_board, int rows, int columns, int i);
int countNeighbors(int n1, int n2, int n3, int n4, int n5, int n6, int n7, int n8,char* game_board, int rows, int columns );
void liveOrDie(int num_neighbors, char* game_board, int rows, int columns, int i, int* kill, int* birth, int*k, int*b);
void updateBoard(int* kill, int*birth, char* game_board);
void timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);


int main(int argc, char *argv[]) {

	if(argc != 3){
		printf("Error on command line format \n");
		printf("usage: ./gol filename display_bit \n");
		printf("note: for display_bit: \n \t 1 = display each iteration \n\t 0 = display final board \n");
		exit(1);
	}
	
	/*Program variables*/
	int rows;
	int columns;
	int num_iterations;
	int num_initial_elements;
	int display;
	
	/*Indicies = Index values for 1D array simulating coordinate pairs*/
	/*Read from file to get initial condition values*/
	int* indicies = getInitialConditions(argv, &rows, &columns, &num_iterations, &num_initial_elements, &display);	
	if(indicies == NULL){ 
		printf("Error on calloc"); 
		exit(1);
	}
	
	/*Create Board*/
	char* game_board = initializeBoard(rows, columns, indicies, num_initial_elements);
	if(game_board== NULL){
		printf("Error on calloc"); 
		exit(1);
	}
	printBoard(rows,columns, game_board);
	
	int* kill = calloc(rows*columns, sizeof(int));
	int* birth = calloc(rows*columns, sizeof(int));
	if(kill == NULL || birth == NULL){
		printf("Error on calloc");
		exit(1);
	}
	
	int iteration = 0;
	int num_neighbors;
	
	/*Get Start Time*/
	int ret;
	struct timeval begin;
	ret = gettimeofday(&begin, NULL);
	if(ret <0){
		printf("Error on gettimeofday \n");
		exit(0);
	}
	
	/*RUN GAME OF LIFE*/
	while(iteration < num_iterations){
		int b = 0; //birth
		int k = 0; //kill
		/*Scan throuh entire board
		 *Find neighbors of each cell
		 *Count live neighbors in around each cell
		 *Determine if cell should live or die depending on number of live neighbors
		 *Store cells to kill/revive in different arrays
		 */
		 int cell;
		 for(cell = 0; cell < rows*columns; cell++){//scan through entire board
		 	int n1, n2, n3, n4, n5, n6, n7, n8;
			eightNeighbors(&n1, &n2, &n3, &n4, &n5, &n6, &n7, &n8, game_board, rows,columns, cell);
			num_neighbors =  countNeighbors(n1, n2, n3, n4, n5, n6, n7, n8, game_board, rows, columns);
			liveOrDie(num_neighbors, game_board, rows, columns, cell, kill, birth, &k, &b);
		}
		
		/*Update board after each iteration*/
		updateBoard(kill,birth,game_board);
		
		/*Clear arrays storing cells to kill and revive after each iteration*/	
		int z;
		for(z = 0; z<rows*columns; z++){
			*(birth + z) = 0;
			*(kill + z) = 0;		
		}

		/*Print board after each iteration*/
		if(display == 1){
			system("clear");
			printf("Time Step: %d \n", iteration+1);
			printBoard(rows,columns, game_board);
			usleep(15000);
		}
		iteration++;
	}
	/*END RUN GAME OF LIFE*/
	
	/*Get End Time*/
	struct timeval end;
	ret = gettimeofday(&end, NULL);
	if(ret <0){
		printf("Error on gettimeofday \n");
		exit(0);
	}
	
	/*Compute total time*/
	struct timeval result;
	timeval_subtract (&result, &end, &begin);
	//printf("Run Time: %ld.%06ld seconds \n", result.tv_sec, result.tv_usec);
	if (display == 0) {
		printf("Final board:  \n");
		printBoard(rows,columns, game_board);
	}
	
	free(kill);
	free(birth);
	free(indicies);
	free(game_board);
	return 0;
}




/* Subtract the �struct timeval� values X and Y, storing the result in RESULT.
 * @url https://www.gnu.org/software/libc/manual/html_node/Elapsed-Time.html
 */
void timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y){
        /* Perform the carry for the later subtraction by updating y*/
        if (x->tv_usec < y->tv_usec) {
                int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
                y->tv_usec -= 1000000 * nsec;
                y->tv_sec += nsec;
        }
        if (x->tv_usec - y->tv_usec > 1000000) {
                int nsec = (x->tv_usec - y->tv_usec) / 1000000;
                y->tv_usec += 1000000 * nsec;
                y->tv_sec -= nsec;
        }

        /* Compute the time remaining to wait.tv_usec is certainly positive*/
        result->tv_sec = x->tv_sec - y->tv_sec;
        result->tv_usec = x->tv_usec - y->tv_usec;
}






/*Reads from input file
 * Sets board parameters read from file via pointers
 * Returns array of "live" indicies for initial board
 * @param argv : command line arg
 * @param rows : # rows in game board
 * @param columns: #columns in game board
 * @param num_iterations: # times to run game
 * @param num_initial_elements: number of initial "live" elements
 * @param display: bit (0 or 1) indicating whether or not to display
 * 	the board after each iteration
 * @return indicies: array of "live" indicies for initial board
 */
int* getInitialConditions(char* argv[], int* rows, int* columns, int* num_iterations, int* num_initial_elements, int* display){
	char* filename = argv[1];
	FILE* infile;
	infile = fopen(filename, "r");
	if(infile == NULL){
		printf("Error: file open\n");
		exit(1);
	}
	
	fscanf(infile, "%d %d %d %d", rows, columns, num_iterations, num_initial_elements);
	
	int* coordinates = calloc(2*(*num_initial_elements), sizeof(int));
	int i;
	for(i = 0; i < 2*(*num_iterations); i++){
		int coordinate;
		if(fscanf(infile, "%d",&coordinate) == 1){
			*(coordinates + i) = coordinate;
		}
	}
	
	fclose(infile);
	
	int* indicies = calloc(*num_initial_elements, sizeof(int));
	if(coordinates == NULL || indicies == NULL) { printf("Error on calloc"); exit(1);}
	
	int j, k;
	for(j = 0, k = 0; j< *num_initial_elements; j++, k+=2){
		*(indicies + j) = (((*columns) * ((*(coordinates + k)))) + (*(coordinates + (k+1))));
	}
	free(coordinates);
	
	*display = strtol(argv[2], NULL, 10);
	
	return indicies;
}

/*
 * Creates 1D array of characters to represent game board
 * "live" elements are @
 * "dead" elements are .
 * @param rows: rows in game board
 * @param columns: columns in game board
 * @param indicies: int array storing indicies of "live" elements
 * @param num_initial_elements: number of intial life elements
 * @return game_board: 1D character array representing life board
 */
char* initializeBoard(int rows, int columns, int* indicies, int num_initial_elements)	{
	char* game_board = malloc(rows*columns*sizeof(char));
	int i;
	for(i = 0; i< rows*columns; i++){
		*(game_board + i) = '.';
	}

	int j;
	for(j = 0; j<num_initial_elements; j++){
		*(game_board +  (*(indicies + j))) = '@';
	}
	
	return game_board;

}

/*Prints board*/
void printBoard(int rows, int columns, char* game_board){
	int i;
	for(i = 0; i < rows*columns; i++){
		if(i%columns == 0) printf("\n");
		printf("%c ", *(game_board + i));
	}
	
	printf("\n");
}

/*Finds 8 neighbors of each element in array
 * Considers (1) middle of board, (2) edges of board (3) corners of board
 * @param n1-n8 index values of neighbors
 * @param game_board game board
 * @param rows, columns: rows, columns in game board
 * @int i iterator through game board
 */
void eightNeighbors(int* n1, int* n2, int* n3, int* n4, int* n5, int* n6, int* n7, int* n8, char* game_board, int rows, int columns, int i){
	*n1 = *n2 = *n3 = *n4 = *n5 = *n6 = *n7 = *n8 = 0;
	if( (i%columns != 0) && (((i+1) % columns) != 0) && (i > columns) && (i < (rows*columns - columns))) //middle (no edges)
	{
		*n1 = i + 1; 
		*n2 = i - 1;
		*n3 = i + columns;
		*n4 = i - columns;
		*n5 = i + columns + 1;
		*n6 = i + columns - 1;
		*n7 = i - columns + 1;
		*n8 = i - columns - 1;
	}

	else if(i > 0 && i <(columns-1)) //top row(not corner)
	{
		*n1 = i + 1;
		*n2 = i - 1;
		*n3 = i + columns;
		*n4 = i + (columns*rows - columns);
		*n5 = i + columns + 1;
		*n6 = i + columns - 1;
		*n7 = *n4 +1;
		*n8 = *n4 -1;
	}

	else if(i> rows*columns - columns && i<rows*columns -1) //bottom row(not corner)
	{                        
		*n1 = i + 1;
		*n2 = i - 1;
		*n3 = i - (columns*rows - columns); 
		*n4 = i - columns;
		*n5 = *n3 + 1; 
		*n6 = *n3 - 1; 
		*n7 = i - columns + 1;
		*n8 = i - columns - 1;

	}

	else if((i%columns == 0) && (i != 0) && (i!= rows*columns - columns)) //first columns (not corner)
	{	
		*n1 = i + 1;
		*n2 = i - 1;
		*n3 = i + columns;
		*n4 = i - columns;
		*n5 = i + columns + 1;
		*n6 = i + columns - 1;
		*n7 = i - columns + 1;
		*n8 = i -1 + 2*columns;

	}	

	else if( ((i+1)% columns == 0) && (i != columns -1) && (i != rows*columns -1)) //last column (not corner)
	{
		*n1 = i + 1;
		*n2 = i - 1;
		*n3 = i + columns;
		*n4 = i - columns;
		*n5 = i - 2*columns + 1;
		*n6 = i + columns - 1;
		*n7 = i - columns + 1;
		*n8 = i - columns - 1;
	}


	if(i == 0)//top left corner
	{
		*n1 = i + 1;
		*n2 = i + 2*columns -1; 
		*n3 = i + columns;
		*n4 = (rows*columns) - columns + 1;
		*n5 = i + columns + 1;
		*n6 = i + columns - 1;
		*n7 = (rows*columns) - columns; 
		*n8 = (rows*columns) -1; 
	}
	if(i == columns -1) //top right corner
	{
		*n1 = i + 1;
		*n2 = i - 1;
		*n3 = i + columns;
		*n4 = (rows*columns) - columns; 
		*n5 = (rows*columns) -1; 
		*n6 = i + columns - 1;
		*n7 = 0;
		*n8 = *n5 - 1; 
	}


	if(i == (columns*rows - columns)) //bottom left corner
	{
		*n1 = i + 1;
		*n2 = i - 1;
		*n3 = 0;
		*n4 = i - columns;
		*n5 = columns -1; 
		*n6 = i + (columns - 1);
		*n7 = i - columns + 1;
		*n8 = 1;
	}
	if(i == (rows*columns -1)) //bottom right corner
	{
		*n1 = i - 2*columns + 1; 
		*n2 = i - 1;
		*n3 = 0;
		*n4 = i - columns;
		*n5 = columns -2;
		*n6 = columns -1;
		*n7 = i - columns + 1;
		*n8 = i - columns - 1;
	}	



}

/* Counts number of "live" neighbors of each element in array
 * Considers (1) middle of board, (2) edges of board (3) corners of board
 * @param n1-n8 index values of neighbors
 * @param game_board game board
 * @param rows, columns: rows, columns in game board
 * @int i iterator through game board
 */
int countNeighbors(int n1, int n2, int n3, int n4, int n5, int n6, int n7, int n8,char* game_board, int rows, int columns )
{	
	int num_neighbors = 0;
	if(( *(game_board + n1)) == '@') {num_neighbors++;}
	if(( *(game_board + n2)) == '@') {num_neighbors++;}
	if(( *(game_board + n3)) == '@') {num_neighbors++;}
	if(( *(game_board + n4)) == '@') {num_neighbors++;}
	if(( *(game_board + n5)) == '@') {num_neighbors++;}
	if(( *(game_board + n6)) == '@') {num_neighbors++;}
	if(( *(game_board + n7)) == '@') {num_neighbors++;}
	if(( *(game_board + n8)) == '@') {num_neighbors++;}
	return num_neighbors;	
}


/*Implements live or die rules
 * Fills arrays with values to update
 * 	kill array - indicies to kill
 * 	birth array - indicies to revive
 * Live cell with 0 or 1 live neighbors dies
 * Live cell with more than 4 neighbors dies
 * Dead cell with 3 neighbors becomes alive
 *
 * @param kill and birth: arrays to store values to update
 * @param k and b: iterators through kill and birth
 */
void liveOrDie(int num_neighbors, char* game_board, int rows, int columns, int cell, int* kill, int* birth, int*k, int*b){	

	if( (*(game_board + cell)) == '@'){
		if((num_neighbors <= 1) || (num_neighbors >= 4)) {	
			(*(kill + (*k))) = cell;
			(*k)++;
		}
	}

	if( (*(game_board + cell)) == '.'){
		if(num_neighbors == 3){
		    (*(birth+(*b))) = cell;
			(*b)++;
		}
	}

	
}		

/*Updates board after each iteration
 * Reads from kill and birth arrays to update board
 */
void updateBoard(int* kill, int*birth, char* game_board){

	int l=0;
	if( (*birth) == 0) {
	 	*(game_board + ((*(birth+ l)))) = '@';
                l++; 
	}

	while( (*(birth + l)) != 0){
		*(game_board + ((*(birth+ l)))) = '@';
		l++;
	}	

	
	int d = 0;
	if( (*kill) == 0){
		*(game_board + ((*(kill + d)))) = '.';
		d++;
	}


	while( (*(kill + d)) != 0){
		*(game_board + ((*(kill + d)))) = '.';
		d++;
	}	

}

